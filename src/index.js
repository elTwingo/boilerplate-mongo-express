import express from 'express';
import { MongoClient } from 'mongodb';

const app = express();
const port = 3000;

app.get('/', (req, res) => {
  res.send('Hello World!')
})

const url = 'mongodb://db:27017';
const client = new MongoClient(url);

const dbName = 'test';

async function main() {
  await client.connect();
  console.log('Connected successfully to server');
  const db = client.db(dbName);
  const collection = db.collection('documents');
  return 'done.';
}

main()
  .then(console.log)
  .catch(console.error)
  .finally(() => client.close());

app.listen(port, () => console.log('ready!'));